<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Model extends CI_Model 
{
    protected $model;

    public function __construct()
    {
        $this->model = explode('_', get_class($this))[1];
    }

    public function find_all($columns = '*') # : mixed array
    {
        $this->db->select($columns);
        $this->db->from($this->model);
        
        return $this->to_array($this->db->get());
    }

    public function count() # : int
    {
        return $this->db->count_all($this->model); 
    }

    public function find_by($column, $value, $columns = '*') # : mixed array
    {
        $this->db->select($columns);
        $this->db->from($this->model);
        $this->db->where($column, $value);

        return $this->to_array($this->db->get());
    }

    public function find_by_id($id, $columns = '*') # : mixed
    {
        $this->db->select($columns)->from($this->model);
        $this->db->where($this->get_column_name('id'), $id);
        $query = $this->db->get();
        if ($query->num_rows() != 1)
        {
            return NULL;
        }
        $result = $query->result();
        return  array_shift($result);
    }

    public function save($data) # : bool
    {
        return $this->db->insert($this->model, $data);
    }

    public function update($column, $value, $data) # : bool
    {
        if ($column == NULL) 
        {
            $column = $this->get_column_name('id');
        }
        $this->db->where($column, $value);
        return $this->db->update($this->model , $data);
    }

    public function delete($column, $value)  # : mixed
    {
        if ($column == NULL) 
        {
            $column = $this->get_column_name('id');
        }
        return $this->db->where($column, $value)->delete($this->model);
    }

    protected function get_column_name($column) # : string
    {
        $model =  rtrim($this->model, 's');
        return sprintf('%s_%s', $model, $column);
    }

    protected function to_array($query) # : mixed array
    {
        if ($query->num_rows() > 0) 
        {
            return $query->result();
        }
        else
        {
            return array();
        }
    }
}	