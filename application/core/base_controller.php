<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Controller extends CI_Controller {
    
	public function __construct ()
	{
		parent::__construct();
	}

    protected function http_matches($methods, $url = NULL) 
    {
        if ($methods == '*' || $methods == 'any' || $methods == 'ANY')
        {
            return;
        }

        if (!is_array($methods))
        {
            $methods = [$methods];
        }
        
        $methods = array_map('strtoupper', $methods);
        $current_method = $this->input->method(TRUE);

        if (!in_array($current_method, $methods)) 
        {
            redirect($url != NULL ? $url : 404);
        }
    }

}
