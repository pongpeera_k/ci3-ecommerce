<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_products extends Base_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function dis_products()
	{
			$this->db->distinct();
			$query = $this->db->query('SELECT DISTINCT product_name FROM products');
			return $query->result();
	}
		
	public function report($report_products)
	{
		
		$this->db->insert('reports',$report_products);
		
	}
	
	public function reports()
	{ 
		$report = $this->db->get('reports');
		if($report->num_rows() > 0 ) {
			return $report->result();
		} else {
			return array();
		}
		
	}
	
	public function del_report($rep_id_product)
	{
		$this->db->where('rep_id_product',$rep_id_product)
		->delete('reports');
	}
}