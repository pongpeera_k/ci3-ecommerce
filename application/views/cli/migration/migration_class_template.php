defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_{name} extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                '{name}_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('{name}_id', TRUE);
        $this->dbforge->create_table('{name}');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('{name}');
    }

    private function initialized_data()
    {
        
    }
}