<?php $this->load->view('admin/header', [ 'title' => $title ]) ?>
	<!-- /.row -->
	<div class="row">
		<!-- body items -->

		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><i class="fa fa-fw fa-compass"></i> Settings</h4>
				</div>
				<div class="panel-body"><?= validation_errors()?>
					<div class="col-md-12">
								<blockquote>
									<p>Here You Can <code>Edit</code> Site Information  and <code>Add</code> Some Options: </p>

									
									<blockquote>
										<p><kbd>Change</kbd> Admin Password </p>
					<?=  form_open('admin/settings/change_password_admin',['class'=>'form-inline']) ?>	
									<div class="col-md-3">
									<div class="input-group">
										<input type="text" class="form-control" name="oldpassword_admin" placeholder="Enter Old Password"  value="<?=set_value('oldpassword_admin') ?>">
									</div>
									</div>
									<div class="col-md-3">
									<div class="input-group">
										<input type="text" class="form-control" name="password_admin" placeholder="New Admin Password"  value="<?=set_value('password_admin') ?>">
									</div>
									</div>
									<div class="col-md-3">
									<div class="input-group">
										<input type="text" class="form-control" name="repassword_admin" placeholder="Re Type Your Password" value="<?=set_value('repassword_admin') ?>">
									</div>
									</div>
					<button type="submit" class="btn btn-success btn-xs" onclick="return confirm ('Are You Sure You Want Change  Admin Password  ?')">Change</button>
					<?= form_close() ?>
									</blockquote>
									<blockquote><p><kbd>Add</kbd> Slide Show ...</p></blockquote>
								</blockquote>
							
						
					</div><!-- /.div 12 -->
				</div><!-- /.div panel-body  -->
			</div><!-- /.div  panel-default -->
		</div> <!-- /.div 12 -->
	</div>
	<!-- /.row -->
<?php $this->load->view('admin/footer') ?>
			