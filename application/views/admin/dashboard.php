<?php $this->load->view('admin/header', [ 'title' => $title ])?>
<!-- /.row -->
<div class="row">
	<!-- body items -->

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<i class="fa fa-fw fa-compass"></i> 
					Products 
					<?=  anchor('admin/products/create','Add New Product',['class'=>'btn btn-primary btn-xs']) ?>
				</h4>
				
			</div>
			<div class="panel-body">
				<table class="table table-striped table-hover" id="tableproducts">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Title - Product</th>
							<!--<th>Description</th>-->
							<th>Price</th>
							<th>Stock</th>
							
							<th>Control Product</th>
						</tr>
					</thead>
					<tbody>
					<!-- load products from table -->
					<?php foreach ($products as $product ) : ?>
						<tr>
							<td><?=  $product->product_id  ?></td>
							<td>
								<a href="">
									<style>#g {width:50px;height:50px;}</style>
									<?php
										
										$product_image =['src'	=>'assets/uploads/'.$product->product_image,
										
										'class'=>'img-responsive img-portfolio img-hover',
										'id'=>'g'
										];
										echo img($product_image);
									?>
							</td>
							<td><?=  $product->product_title.' '.$product->product_name  ?></td>
							<td><?=  $product->product_price  ?></td>
							<td><?=  $product->product_stock  ?></td>
							
							<td>
								<?=  anchor('admin/products/edit/'.$product->product_id,'Edit',['class'=>'btn btn-success btn-xs']) ?>
								<?php  if($this->session->userdata('group')	==	'1' ): ?>
								<?=  anchor('admin/products/delete/'.$product->product_id,'Delete',['class'=>'btn btn-danger btn-xs',
																								'onclick'=>'return confirm(\'Are You Sure ? \')'
																								]) ?>
								<?php else:?>
								<?=  anchor('admin/products/delete/','Delete',['class'=>'btn btn-danger btn-xs','data-toggle'=>'button',
									'onclick'=>'return confirm(\'Sorry You Cant Delete it you should be admin  ? \')'
								]) ?>
								<?php endif;?>
								
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#tableproducts').DataTable();
						
					});
				</script>
			</div>
		</div>
	</div> 
</div>
<!-- /.row -->
<?php $this->load->view('admin/footer')?>			