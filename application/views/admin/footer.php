<?php $templateUrl = base_url('assets/templates/admin'); ?>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= $templateUrl; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= $templateUrl; ?>/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?= $templateUrl; ?>/vendor/raphael/raphael.min.js"></script>
    <script src="<?= $templateUrl; ?>/vendor/morrisjs/morris.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= $templateUrl; ?>/js/sb-admin-2.js"></script>

</body>

</html>