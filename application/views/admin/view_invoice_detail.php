<?php $this->load->view('admin/header', [ 'title' => $title ]) ?>
	<!-- /.row -->
	<div class="row">
		<!-- body items -->

		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><i class="fa fa-fw fa-compass"></i> Items Ordered in Invoice # <?php foreach ($invoice as $invoice_id ) : ?><?= $invoice_id->id ?>	<?php endforeach; ?> </h4>
					
				</div>
				<div class="panel-body">
					
					<table class="table table-striped table-hover" id="table-invoice">
						<thead>
							<tr>
								
								
								<th>Product id</th>
								<th>Product type</th>
								<th>Product title</th>
								<th>Quantity</th>
								<th>price</th>
								<th>Subtotal</th>
								
							</tr>
						</thead>
						<tbody>
						<!-- load products from table -->
						<?php 
							$total = 0;
							foreach ($orders as $order ) : 
							$subtotal = $order->qty * $order->price;
							$total += $subtotal;
						?>
							<tr>
								<td><?= $order->product_id ?></td>	
								<td><?= $order->product_type ?></td>
								<td><?= $order->product_title ?></td>
								<td><?= $order->qty ?></td>
								<td><?= $order->price ?></td>
								<td><?= $subtotal ?></td>
							
							</tr>
							<?php endforeach; ?>
							<tfoot>
								<tr>
									<td align="right" colspan="5">Total :</td>
									<td><?=$total?></td>
								</tr>
							</tfoot>
							
						</tbody>
					</table>
					<script>
						$(document).ready(function(){
							$('#table-invoice').DataTable();
							
						});
					</script>
					
				</div>
			</div>
		</div> 
		
	</div>
	<!-- /.row -->
<?php $this->load->view('admin/footer') ?>
