<?php $this->load->view('admin/header', [ 'title' => $title ]) ?>
	<!-- /.row -->
	<div class="row">
		<!-- body items -->

		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><i class="fa fa-fw fa-compass"></i> Invoices List </h4>
					
				</div>
				<div class="panel-body">
					<table class="table table-striped table-hover" id="table-invoice">
						<thead>
							<tr>
								<th>id</th>
								<th>data</th>
								<th>due_date</th>
								<th>status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						<!-- load products from table -->
						<?php foreach ($invoices as $invoice ) : ?>
							<tr>
								<td><?=  $invoice->id  ?></td>
								<td><?=  $invoice->data  ?></td>
								<td><?=  $invoice->due_date  ?></td>
								<td><?=  $invoice->status  ?></td>
								<?php if($invoice->status == 'confirmed'):?>
								<td><?=  anchor('admin/invoices/detail/'.$invoice->id,'Details',['class'=>'btn btn-success btn-xs']) ?>
								<?php else:?>
								<td><?=  anchor('admin/invoices/detail/'.$invoice->id,'Details',['class'=>'btn btn-primary btn-xs']) ?>
								<?php endif;?>
								</td>
							</tr>
							<?php endforeach; ?>
							
							
						</tbody>
					</table>
					<script>
						$(document).ready(function(){
							$('#table-invoice').DataTable();
							
						});
					</script>
					
				</div>
			</div>
		</div> 
		
	</div>
	<!-- /.row -->
<?php $this->load->view('admin/footer') ?>
