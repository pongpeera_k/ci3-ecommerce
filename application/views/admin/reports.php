<?php $this->load->view('admin/header', [ 'title' => $title ]) ?>
	<!-- /.row -->
	<div class="row">
		<!-- body items -->

		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><i class="fa fa-fw fa-compass"></i> Report</h4>
					
				</div>
				<div class="panel-body">
				<?php foreach ($reports as $report) : ?>
				<div class="col-md-12">
				<hr>

					<div class="col-md-2">
						<h4>id Product</h4>
						<?=  $report->report_id_product  ?>
					</div>
					
					<div class="col-md-2">
						<h4>Name Product</h4>
						<?=  $report->report_name  ?>
					</div>
					
					<div class="col-md-2">
						<h4>Title Product</h4>
						<?=  $report->report_title_product  ?>
					</div>
					
					<div class="col-md-2">
						<h4>Name User</h4>
						<?=  $report->report_user_name  ?>
					</div>
					
					<div class="col-md-2">
						<h4>Group User</h4>
						<?=  $report->report_user_group  ?>
					</div>
					<div class="col-md-2">
						<h4>View The Product</h4>
						<?=  anchor('admin/products/edit/'.$report->report_id_product,'View',['class'=>'btn btn-success btn-xs'])  ?>
						<?php  if($this->session->userdata('group')	==	'1' ): ?>
						<?=  anchor('admin/products/del_report/'.$report->report_id_product,'Delete Report',['class'=>'btn btn-danger btn-xs',
							'onclick'=>'return confirm(\'Are You Sure You Want Delete The Report? \')'
						])  ?>
						<?php else:?>
						<?=  anchor('admin/products/del_report/','Delete Report',['class'=>'btn btn-danger btn-xs','data-toggle'=>'button',
							'onclick'=>'return confirm(\'Sorry You Can Just Edit , You Should be Admin To Delete it \')'
						])  ?>
						<?php endif;?>
					</div>
				<hr>
				</div>
				<?php endforeach; ?>	
				</div>
			</div>
		</div> 
		
	</div>
	<!-- /.row -->
<?php $this->load->view('admin/footer') ?>
