<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_users');
		$this->load->model('model_settings');
	}

    public function register()
    {
		$this->http_matches('GET');
        $data['get_sitename'] = $this->model_settings->sitename_settings();
        $data['get_footer'] = $this->model_settings->footer_settings();	
        $this->load->view('account/form_register',$data); 
    }

    public function signup()
    {
		$this->http_matches('POST');
        $this->form_validation->set_rules('rusername','Username','required|alpha_numeric|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('rpassword','Password','required|alpha_numeric|min_length[6]|max_length[60]|md5');
		$this->form_validation->set_rules('repassword','Password','required|alpha_numeric');
        if (!$this->form_validation->run())
        {
			# Invalid input
			redirect('account/register');
		}

		$users = $this->model_users->find_by('user_name', set_value('rusername'));
		if (count($users) > 0)
		{
			# duplicate username
			redirect('account/register');
		}

		$register_data = [
			'user_name' => set_value('rusername'),
			'user_password' => set_value('rpassword'),
			'status' => 0,
			'user_group' => 3
		];
		$created = $this->model_users->save($register_data);
		if (!$created) 
		{
			# can't save
			redirect('account/register');
		}
		$this->session->set_userdata('username', set_value('rusername'));
		$this->session->set_userdata('group', 3);
		redirect(base_url());
	}

	public function login()
	{
		$this->http_matches('GET');
		$data['get_sitename'] = $this->model_settings->sitename_settings();
		$data['get_footer'] = $this->model_settings->footer_settings();	
		$this->load->view('account/form_login',$data); 
	}

	public function signin()
	{
		$this->http_matches('POST');
		$this->form_validation->set_rules('username','Username','required|alpha_numeric');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric|md5');
		
		if(!$this->form_validation->run())
		{
			// invalid input
			redirect('account/login');
		}

		$this->load->model('model_users');	
		$users = $this->model_users->find_by('user_name', set_value('username'));
		$user =  array_shift($users);

		if ($user == NULL || !($user->user_password == set_value('password')) )
		{
			// username or password not correct !
			redirect('account/login');
		}

		if ($user->status != 1) 
		{
			// user is not active
			redirect('account/login');
		}

		$this->session->set_userdata('username',$user->user_name);
		$this->session->set_userdata('group',$user->user_group);
		
		if ($user->user_group == 1 || $user->user_group == 2) 
		{
			redirect('admin/products');
		}
		else 
		{
			redirect(base_url());
		}
	}
	
	public function logout()
	{
		$this->http_matches('GET');
		$this->session->sess_destroy();
		redirect('account/login');
	}
}