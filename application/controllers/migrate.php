<?php
class Migrate extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->input->is_cli_request())
        {
            show_error($this->migration->error_string());
        }
    }

    public function generate($name = NULL)
    {
        if ($name == NULL) 
        {
            echo 'Invalid migration\'s name.'.PHP_EOL ;
            return;
        }
        $this->load->library('parser');
        $date = new DateTime();
        $table_name = strtolower($name);
        $path = sprintf('%smigrations\\%s_%s.php', APPPATH, $date->format('YmdHis'), $table_name);
        $file_writer = fopen($path, "w") or die("Unable to create migration file!");
        $template = '<?php'.PHP_EOL.$this->parser->parse('cli/migration/migration_class_template', [ 'name' => $table_name ], TRUE);

        fwrite($file_writer, $template);
        fclose($file_writer);
        echo 'created '.$path.PHP_EOL ;
    }

    public function make($verion = NULL)
    {
         $this->load->library('migration');
        if ($verion != NULL) 
        {
            $this->migrate_version($verion);
        }
        else
        {
            $this->load->helper('directory');
            $map = directory_map(APPPATH.'migrations/');
            foreach ($map as $filename) {
                if (is_string($filename)) {
                    $version = explode('_',$filename)[0];
                    $this->migrate_version($verion);
                }
            }
        }
    }

    private function migrate_version($verion) {
        echo $this->migration->version($verion) ? 'migration'.$verion.' done'.PHP_EOL : $this->migration->error_string() ;
    }
	
}//end  class