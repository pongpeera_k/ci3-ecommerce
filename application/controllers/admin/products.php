<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Base_Controller 
{
	private $image_config;
	public function __construct ()
	{
		parent::__construct();
		if($this->session->userdata('group') !=	('1' ||'2') )
		{
			$this->session->set_flashdata('error','Sorry You Are Not Logged in !');
			redirect('account/login');	
		}
		$this->load->model('model_products');
		$this->image_config = [
			'upload_path' => './assets/uploads/',
			'allowed_types' => 'jpg|png',
			'max_size' => 2048000,
			'max_width' => 2000,
			'max_height' => 2000
		];
	}
	
	public function index()
	{
		$this->http_matches('GET');
		$data = [
			'title' => 'Products',
			'products' => $this->model_products->find_all()
		];
		$this->load->view('admin/dashboard',$data);
	}
	
	public function create()
	{
		$this->http_matches('GET');
		$data = [
			'title' => 'Add new product'
		];
		
		$this->load->view('admin/form_create_product', $data);	
		
	}///end class create ///

	public function store()
	{
		$this->http_matches('POST');
		$this->form_validation->set_rules('product_name','Product Name','required');
		$this->form_validation->set_rules('product_title','Product Title','required');
		$this->form_validation->set_rules('product_description','Product Description','required');
		$this->form_validation->set_rules('product_price','Product Price','required|integer');
		$this->form_validation->set_rules('product_stock','Available Stock','required|integer');

		if (!$this->form_validation->run()) 
		{
			# Invalid input.
			redirect('admin/products/create');
		}

		$this->load->library('upload', $this->image_config);
		
		if (!$this->upload->do_upload())
		{
			# upload failed.
			$this->load->view('admin/products/create');
		}	
			
		$upload_image = $this->upload->data();
		$product = [
			'product_name'	=> set_value('product_name'),
			'product_title'	=> set_value('product_title'),
			'product_description' => set_value('product_description'),
			'product_price'	=> set_value('product_price'),
			'product_stock'	=> set_value('product_stock'),
			'product_image'	=> $upload_image['file_name']
		];
		
		$created = $this->model_products->save($product);
		
		if (!$created) 
		{
			# create product failed.
			redirect('admin/products/create');
		}

		redirect('admin/products');	
	}
	
	public function edit($product_id)
	{
		$this->http_matches('GET');
		$product = $this->model_products->find_by_id($product_id);
		$data = [
			'title' => $product->product_title.'-'.$product->product_name,
			'product' => $product
		];
		$this->load->view('admin/form_update_product', $data);
	}

	public function update($product_id)
	{
		$this->http_matches('POST');

		$this->form_validation->set_rules('product_name','Product Name','required|alpha_numeric');
		$this->form_validation->set_rules('product_title','Product Title','required|alpha_numeric');
		$this->form_validation->set_rules('product_description','Product Description','required|alpha_numeric');
		$this->form_validation->set_rules('product_price','Product Price','required|integer');
		$this->form_validation->set_rules('product_stock','Available Stock','required|integer');

		if (!$this->form_validation->run())
		{
			redirect('admin/products/edit/'.$product_id);
		}

		$product = [
			'product_name' => set_value('product_name'),
			'product_title'	=> set_value('product_title'),
			'product_description' => set_value('product_description'),
			'product_price'	=> set_value('product_price'),
			'product_stock' => set_value('product_stock')
		];
		
		if($_FILES['userfile']['name'] != '')
		{
			$this->load->library('upload', $this->image_config);
			if (!$this->upload->do_upload())
			{	
				redirect('admin/products/edit'.$product_id);
			}
			$image = $this->upload->data();
			$product['product_image'] = $image['orig_name'];
		}

		$edited = $this->model_products->update('product_id', $product_id, $product);	
		
		if (!$edited)
		{
			redirect('admin/products/edit'.$product_id);
		}
		redirect('admin/products');	
	}
	
	public function delete($product_id)
	{
		$this->http_matches('GET');
		$this->model_products->delete('product_id', $product_id);
		redirect('admin/products');
	}
	
	public function reports()
	{
		$this->http_matches('GET');
		$data = [
			'title' => 'Reports',
			'reports' => $this->model_products->reports()
		];
		$this->load->view('admin/reports',$data);
	}
	
	public function del_report($rep_id_product)
	{
		$this->http_matches('DELETE');
		$this->model_products->del_report($rep_id_product);
		redirect('admin/products/reports');	
	}
	
}
