<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller 
{
    public function __construct ()
	{
		parent::__construct();
		if($this->session->userdata('group')!=	('1' ||'2') )
		{
			$this->session->set_flashdata('error','Sorry You Are Not Logged in !');
			redirect('account/login');	
		}
		$this->load->model('model_users');
	}

    public function index()
	{
        $data = [
            'title' => 'Members',
            'members' => $this->model_users->members()
        ];
		$this->load->view('admin/members',$data);
	}

	public function active_usr($usr_id)
	{
		$active = '1';
		$data_user = array
		(
			'stuts'			=> $active
		);
		$this->model_users->active($usr_id,$data_user);
		redirect('admin/products/members');
	}

	public function disable_usr($usr_id)
	{
		$active = '0';
		$data_user = array
		(
		'stuts'			=> $active
		);
		$this->model_users->active($usr_id,$data_user);
		redirect('admin/products/members');
	}
}