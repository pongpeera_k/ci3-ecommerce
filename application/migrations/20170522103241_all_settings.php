<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_all_settings extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'all_settings_id' => [
                    'type' => 'INT',
                    'constraint' => 16,
                    'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('all_settings_id', TRUE);
        $this->dbforge->create_table('all_settings');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('all_settings');
    }

    private function initialized_data()
    {
        
    }
}