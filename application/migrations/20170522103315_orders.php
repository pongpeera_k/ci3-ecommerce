<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_orders extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'orders_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('orders_id', TRUE);
        $this->dbforge->create_table('orders');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('orders');
    }

    private function initialized_data()
    {
        
    }
}