<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_users extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'users_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('users_id', TRUE);
        $this->dbforge->create_table('users');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('users');
    }

    private function initialized_data()
    {
        
    }
}