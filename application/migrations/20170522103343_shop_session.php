<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_shop_session extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'shop_session_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('shop_session_id', TRUE);
        $this->dbforge->create_table('shop_session');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('shop_session');
    }

    private function initialized_data()
    {
        
    }
}