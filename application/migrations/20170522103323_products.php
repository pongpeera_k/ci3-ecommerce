<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_products extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'products_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('products_id', TRUE);
        $this->dbforge->create_table('products');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('products');
    }

    private function initialized_data()
    {
        
    }
}