<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_invoices extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'invoices_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('invoices_id', TRUE);
        $this->dbforge->create_table('invoices');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('invoices');
    }

    private function initialized_data()
    {
        
    }
}