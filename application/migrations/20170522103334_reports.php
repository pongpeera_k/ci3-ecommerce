<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_reports extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'reports_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('reports_id', TRUE);
        $this->dbforge->create_table('reports');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('reports');
    }

    private function initialized_data()
    {
        
    }
}