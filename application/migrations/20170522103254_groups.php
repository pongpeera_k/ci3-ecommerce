<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_groups extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
                'groups_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ]
        ]);
        $this->dbforge->add_key('groups_id', TRUE);
        $this->dbforge->create_table('groups');
        $this->initialized_data();
    }

    public function down()
    {
        $this->dbforge->drop_table('groups');
    }

    private function initialized_data()
    {
        
    }
}